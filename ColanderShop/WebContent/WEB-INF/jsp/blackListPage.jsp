<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="utf-8"%>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib prefix="shop_tag" uri="/WEB-INF/tld/shop_taglib.tld" %>   
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>BlackList</title>
<style type="text/css">
  
a.btn {
  border: 1px solid #000000; /* Параметры рамки */
  border-radius: 5px;
  float:right;
  color: #000000; /* цвет текста */
  text-decoration: none; /* убирать подчёркивание у ссылок */
  user-select: none; /* убирать выделение текста */
  background: rgb(255, 255, 255); /* фон кнопки */
  padding: .1em 1em; /* отступ от текста */
  
} 


  </style>
</head>
<body>


<fmt:setLocale value="${sessionScope.local}"/>
<fmt:setBundle basename="localization.local" var="loc"/>
<fmt:message bundle="${loc}" key="local.button.name.ru" var="ru_button" />
<fmt:message bundle="${loc}" key="local.button.name.en" var="en_button" />
<fmt:message bundle="${loc}" key="local.button.name.logout" var="logout" />
<fmt:message bundle="${loc}" key="local.button.name.delete" var="delete" />
<fmt:message bundle="${loc}" key="local.button.name.back_to_main_page" var="back_to_main_page" />
<fmt:message bundle="${loc}" key="local.text.name.user_id" var="user_id" />
<fmt:message bundle="${loc}" key="local.text.name.block_id" var="block_id" />
<fmt:message bundle="${loc}" key="local.text.name.delete_from_black_list" var="delete_from_black_list" />
<fmt:message bundle="${loc}" key="local.text.name.logintext" var="logintext" />

<fmt:message bundle="${loc}" key="local.text.name.success_delete" var="success_delete" />

<form style="float: right;" action="${pageContext.request.contextPath}/controller" method="post">
		<input type="hidden" name="command" value="en"/>
		<input type="submit"  value="${en_button}"/>
</form>
	
<form  style="float: right;" action="${pageContext.request.contextPath}/controller" method="post">
		<input  type="hidden" name="command" value="ru"/>
		<input  type="submit"  value="${ru_button}"/>
</form>

<form  style="float: right;" action="${pageContext.request.contextPath}/controller" method="post">
		<input  type="hidden" name="command" value="logout_command"/>
		<input  type="submit"  value="${logout}"/>
</form>

<br/>
<br/>
<br/>
<a href="${pageContext.request.contextPath}/controller?command=build_admin_page"  class="btn">${back_to_main_page}</a>

<br/>
	<center>
		<c:set var="temp" value="${temp_message}"/>
		<c:if test="${temp !=null}">
			<c:if test="${temp=='success_delete'}">${success_delete} </c:if>

		</c:if>
	</center>
<br/>




<center>
		<table border="1" width="100%" cellpadding="5">
		
		
		
  		 <tr>
  		 <td>${block_id}</td>
  		 <td>${user_id}</td>
    	 <td>${logintext}</td>
    	 <td>${delete_from_black_list}</td>
    	 

   		 </tr>
   		 
   	
   		 <c:if test="${sessionScope.totalUnits==null}">
   		 <c:set var="totalUnits" value="${sessionScope.totalUnits}"/>
   		 </c:if>
   		 
   		 <c:set var="orderList" value="${simpleinfo}"/>
   		 

		 <shop_tag:blackList  blackList="${orderList}"  deleteButton="${delete}"
		  contextPath="${pageContext.request.contextPath}"   unitNumber="${totalUnits}"  >
		 </shop_tag:blackList>
			
			
	

</body>	
</html>