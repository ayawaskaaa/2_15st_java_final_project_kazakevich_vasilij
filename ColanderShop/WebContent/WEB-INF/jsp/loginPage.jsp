<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="utf-8"%>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Login</title>
<style type="text/css">
a.btn {
  border: 1px solid #000000; /* Параметры рамки */
  border-radius: 5px;
  color: #000000; /* цвет текста */
  text-decoration: none; /* убирать подчёркивание у ссылок */
  user-select: none; /* убирать выделение текста */
  background: rgb(255, 255, 255); /* фон кнопки */
  padding: .1em 1em; /* отступ от текста */
  
}  

  </style>
</head>
<body>
<fmt:setLocale value="${sessionScope.local}"/>
<fmt:setBundle basename="localization.local" var="loc"/>
<fmt:message bundle="${loc}" key="local.button.name.ru" var="ru_button" />
<fmt:message bundle="${loc}" key="local.button.name.en" var="en_button" />
<fmt:message bundle="${loc}" key="local.button.name.home" var="home" />
<fmt:message bundle="${loc}" key="local.button.name.register" var="register" />
<fmt:message bundle="${loc}" key="local.button.name.login" var="login" />
<fmt:message bundle="${loc}" key="local.exception.name.authorizationexception" var="authorizationexception" />



<form style="float: right;" action="${pageContext.request.contextPath}/controller" method="post">
		<input type="hidden" name="command" value="en"/>
		<input type="submit"  value="${en_button}"/>
</form>
	
<form  style="float: right;" action="${pageContext.request.contextPath}/controller" method="post">
		<input  type="hidden" name="command" value="ru"/>
		<input  type="submit"  value="${ru_button}"/>
</form>


<center>
<form  action="${pageContext.request.contextPath}/controller" method="post">

		<input  type="hidden" name="command" value="login_command"/>
		<input  type="text" name="login"  value="user1"/>
		<input  type="password" name="password"  value="Password123"/>
		<input  type="submit"  value="${login}"/><br/>
</form>
<br/>
<br/>
<a href="${pageContext.request.contextPath}/controller?command=to_register_page" class="btn">${register}</a>
<a href="http://localhost:8080/ColanderShop/controller?command=build_visitor_page" class="btn">${home}</a>
<br/><br/><br/>

<c:set var="error" value="${errorMessage}"/>
	<font color="red">
	<c:if test="${error !=null}">
	<c:if test="${error=='authorizationexception'}">${authorizationexception} </c:if>
	</c:if>
	</font>
	


</center>
</body>	
</html>