<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="utf-8"%>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib prefix="shop_tag" uri="/WEB-INF/tld/shop_taglib.tld" %>   
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Orders</title>
<style type="text/css">
  
a.btn {
  border: 1px solid #000000; /* Параметры рамки */
  border-radius: 5px;
  float:right;
  color: #000000; /* цвет текста */
  text-decoration: none; /* убирать подчёркивание у ссылок */
  user-select: none; /* убирать выделение текста */
  background: rgb(255, 255, 255); /* фон кнопки */
  padding: .1em 1em; /* отступ от текста */
  
} 


  </style>
</head>
<body>


<fmt:setLocale value="${sessionScope.local}"/>
<fmt:setBundle basename="localization.local" var="loc"/>
<fmt:message bundle="${loc}" key="local.button.name.ru" var="ru_button" />
<fmt:message bundle="${loc}" key="local.button.name.en" var="en_button" />
<fmt:message bundle="${loc}" key="local.button.name.logout" var="logout" />
<fmt:message bundle="${loc}" key="local.button.name.to_orderlist" var="to_orderlist" />
<fmt:message bundle="${loc}" key="local.button.name.list" var="list" />
<fmt:message bundle="${loc}" key="local.button.name.delete" var="delete" />


<fmt:message bundle="${loc}" key="local.button.name.block_state" var="block_state" />
<fmt:message bundle="${loc}" key="local.button.name.change_button" var="change_button" />
<fmt:message bundle="${loc}" key="local.button.name.executed_state" var="executed_state" />
<fmt:message bundle="${loc}" key="local.button.name.in_process_state" var="in_process_state" />
<fmt:message bundle="${loc}" key="local.button.name.set_to_black_list" var="set_to_black_list" />
<fmt:message bundle="${loc}" key="local.button.name.back_to_main_page" var="back_to_main_page" />

<fmt:message bundle="${loc}" key="local.text.name.success_add" var="success_add" />

<fmt:message bundle="${loc}" key="local.text.name.success_change" var="success_change" />

<fmt:message bundle="${loc}" key="local.text.name.item_name" var="item_name" />
<fmt:message bundle="${loc}" key="local.text.name.material" var="material" />
<fmt:message bundle="${loc}" key="local.text.name.size" var="size" />
<fmt:message bundle="${loc}" key="local.text.name.amount" var="amount" />
<fmt:message bundle="${loc}" key="local.text.name.description" var="description" />
<fmt:message bundle="${loc}" key="local.text.name.price" var="price" />
<fmt:message bundle="${loc}" key="local.text.name.image" var="image" />
<fmt:message bundle="${loc}" key="local.text.name.add" var="add" />
<fmt:message bundle="${loc}" key="local.text.name.delete_from_list" var="delete_from_list" />
<fmt:message bundle="${loc}" key="local.text.name.order_id" var="order_id" />
<fmt:message bundle="${loc}" key="local.text.name.order_state" var="order_state" />
<fmt:message bundle="${loc}" key="local.text.name.chosen_amount" var="chosen_amount" />
<fmt:message bundle="${loc}" key="local.text.name.total_price" var="total_price" />
<fmt:message bundle="${loc}" key="local.text.name.user_id" var="user_id" />
<fmt:message bundle="${loc}" key="local.text.name.add_to_black_list" var="add_to_black_list" />
<fmt:message bundle="${loc}" key="local.text.name.change_state" var="change_state" />








<form style="float: right;" action="${pageContext.request.contextPath}/controller" method="post">
		<input type="hidden" name="command" value="en"/>
		<input type="submit"  value="${en_button}"/>
</form>
	
<form  style="float: right;" action="${pageContext.request.contextPath}/controller" method="post">
		<input  type="hidden" name="command" value="ru"/>
		<input  type="submit"  value="${ru_button}"/>
</form>

<form  style="float: right;" action="${pageContext.request.contextPath}/controller" method="post">
		<input  type="hidden" name="command" value="logout_command"/>
		<input  type="submit"  value="${logout}"/>
</form>

<br/>
	<center>
		<c:set var="temp" value="${temp_message}"/>
		<c:if test="${temp !=null}">
			<c:if test="${temp=='success_add'}">${success_add} </c:if>
			<c:if test="${temp=='success_change'}">${success_change} </c:if>

		</c:if>
	</center>
<br/>
<br/>

<a href="${pageContext.request.contextPath}/controller?command=build_admin_page"  class="btn">${back_to_main_page}</a>

<br/><br/>




<center>
		<table border="1" width="100%" cellpadding="5">
		
		
		
  		 <tr>
  		 <td>${user_id}</td>
  		 <td>${order_id}</td>
    	 <td>${item_name}</td>
    	 <td>${image}</td>
    	 <td>${chosen_amount}</td>
    	 <td>${price}</td>
    	 <td>${order_state}</td>
    	 <td>${total_price}</td>
    	 <td>${change_state}</td>
    	 <td>${add_to_black_list}</td>
    	 

   		 </tr>
   		 
   	
   		 <c:if test="${sessionScope.totalUnits==null}">
   		 <c:set var="totalUnits" value="${sessionScope.totalUnits}"/>
   		 </c:if>
   		 
   		 <c:set var="orderList" value="${simpleinfo}"/>
   		 

		 <shop_tag:adminOrderList  adminOrderList="${orderList}" blockState="${block_state}" changeButton="${change_button}"
		  inProcessState="${in_process_state}" contextPath="${pageContext.request.contextPath}" 
		  unitNumber="${totalUnits}" executedState="${executed_state}" setButton="${set_to_black_list}">
		 </shop_tag:adminOrderList>
			
			
	

</body>	
</html>