<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="utf-8"%>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  <%@ taglib prefix="shop_tag" uri="/WEB-INF/tld/shop_taglib.tld" %>   
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Colander Shop</title>
<style type="text/css">
  
a.btn {
  border: 1px solid #000000; /* Параметры рамки */
  border-radius: 5px;
  float:right;
  color: #000000; /* цвет текста */
  text-decoration: none; /* убирать подчёркивание у ссылок */
  user-select: none; /* убирать выделение текста */
  background: rgb(255, 255, 255); /* фон кнопки */
  padding: .1em 1em; /* отступ от текста */
  
} 

  </style>
</head>
<body>

<fmt:setLocale value="${sessionScope.local}"/>
<fmt:setBundle basename="localization.local" var="loc"/>
<fmt:message bundle="${loc}" key="local.button.name.ru" var="ru_button" />
<fmt:message bundle="${loc}" key="local.button.name.en" var="en_button" />
<fmt:message bundle="${loc}" key="local.text.name.greeting" var="greeting" />
<fmt:message bundle="${loc}" key="local.button.name.logout" var="logout" />
<fmt:message bundle="${loc}" key="local.button.name.list" var="list" />
<fmt:message bundle="${loc}" key="local.button.name.to_shoplist" var="to_shoplist" />
<fmt:message bundle="${loc}" key="local.button.name.to_orderlist" var="to_orderlist" />
<fmt:message bundle="${loc}" key="local.text.name.item_name" var="item_name" />
<fmt:message bundle="${loc}" key="local.text.name.material" var="material" />
<fmt:message bundle="${loc}" key="local.text.name.size" var="size" />
<fmt:message bundle="${loc}" key="local.text.name.amount" var="amount" />
<fmt:message bundle="${loc}" key="local.text.name.description" var="description" />
<fmt:message bundle="${loc}" key="local.text.name.price" var="price" />
<fmt:message bundle="${loc}" key="local.text.name.image" var="image" />
<fmt:message bundle="${loc}" key="local.text.name.add" var="add" />
<fmt:message bundle="${loc}" key="local.text.name.success_add" var="success_add" />
<fmt:message bundle="${loc}" key="local.text.name.user_page_info" var="user_page_info" />

<form style="float: right;" action="${pageContext.request.contextPath}/controller" method="post">
		<input type="hidden" name="command" value="en"/>
		<input type="submit"  value="${en_button}"/>
</form>
	
<form  style="float: right;" action="${pageContext.request.contextPath}/controller" method="post">
		<input  type="hidden" name="command" value="ru"/>
		<input  type="submit"  value="${ru_button}"/>
</form>

<form  style="float: right;" action="${pageContext.request.contextPath}/controller" method="post">
		<input  type="hidden" name="command" value="logout_command"/>
		<input  type="submit"  value="${logout}"/>
</form>



<c:set var="message" value="${userMessage}"/>
	<c:if test="${message !=null}"> 
		<c:out value= "${greeting} ${message}"/>
	</c:if>

<br/>
	<center>
		<c:set var="temp" value="${temp_message}"/>
		<c:if test="${temp !=null}">
			<c:if test="${temp=='success_add'}">${success_add} </c:if>
		</c:if>
	</center>

<br/>
<br/>

<form  style="float: right;" action="${pageContext.request.contextPath}/controller" method="get">
		<input  type="hidden" name="command" value="build_order_list"/>
		<input  type="submit"  value="${to_orderlist}"/>
</form>
<c:if test="${sessionScope.shopList!=null}">
<form style="float: right;"  action="${pageContext.request.contextPath}/controller" method="get">
		<input  type="hidden" name="command" value="show_shop_list"/>
		<input align="right"  type="submit"  value="${list}"/>
</form>

</c:if>
<br/>
<br/>
<p align="center"><img src="img/mainPageImage.jpg" alt="Main image"></p>
<br/>

<p align="center" style="font:italic;" >${user_page_info}</p>

<center>
		<table border="0" width="100%" cellpadding="5">
		
		
		
  		 <tr>
    	 <td>${item_name}</td>
    	 <td>${image}</td>
    	 <td>${material}</td>
    	 <td>${size}</td>
    	 <td>${amount}</td>
    	 <td>${description}</td>
    	 <td>${price}</td>
    	 <td>${add}</td>

   		 </tr>
   		 
 
   		 
		 <c:if test="${sessionScope.totalUnits==null}">
   		 <c:set var="totalUnits" value="${sessionScope.totalUnits}"/>
   		 </c:if>
   		 
   		 <c:set var="shopList" value="${simpleinfo}"/>
   		 

		 <shop_tag:shopItemList  shopItemList="${shopList}"
		 contextPath="${pageContext.request.contextPath}" button="${to_shoplist}" unitNumber="${totalUnits}">
		 </shop_tag:shopItemList>

 

</body>	
</html>