<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  <%@ taglib prefix="shop_tag" uri="/WEB-INF/tld/shop_taglib.tld" %> 
 
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Colander Shop</title>
<style type="text/css">
  
a.btn {
  border: 1px solid #000000; /* Параметры рамки */
  border-radius: 5px;
  float:right;
  color: #000000; /* цвет текста */
  text-decoration: none; /* убирать подчёркивание у ссылок */
  user-select: none; /* убирать выделение текста */
  background: rgb(255, 255, 255); /* фон кнопки */
  padding: .1em 1em; /* отступ от текста */
  
} 

textarea {
    width: 100%; /* Ширина в процентах */
    height: 100px; /* Высота в пикселах */
   }

  </style>
</head>
<body>



<fmt:setLocale value="${sessionScope.local}"/>
<fmt:setBundle basename="localization.local" var="loc"/>
<fmt:message bundle="${loc}" key="local.button.name.ru" var="ru_button" />
<fmt:message bundle="${loc}" key="local.button.name.en" var="en_button" />
<fmt:message bundle="${loc}" key="local.text.name.greeting" var="greeting" />
<fmt:message bundle="${loc}" key="local.button.name.logout" var="logout" />
<fmt:message bundle="${loc}" key="local.button.name.list" var="list" />
<fmt:message bundle="${loc}" key="local.button.name.to_shoplist" var="to_shoplist" />
<fmt:message bundle="${loc}" key="local.button.name.delete" var="delete" />
<fmt:message bundle="${loc}" key="local.button.name.update" var="update" />
<fmt:message bundle="${loc}" key="local.button.name.add_item" var="add_item" />
<fmt:message bundle="${loc}" key="local.button.name.to_admin_orders_list" var="to_admin_orders_list" />
<fmt:message bundle="${loc}" key="local.button.name.to_black_list" var="to_black_list" />


<fmt:message bundle="${loc}" key="local.text.name.success_add" var="success_add" />
<fmt:message bundle="${loc}" key="local.text.name.success_delete" var="success_delete" />
<fmt:message bundle="${loc}" key="local.text.name.success_change" var="success_change" />

<fmt:message bundle="${loc}" key="local.text.name.item_name" var="item_name" />
<fmt:message bundle="${loc}" key="local.text.name.material" var="material" />
<fmt:message bundle="${loc}" key="local.text.name.size" var="size" />
<fmt:message bundle="${loc}" key="local.text.name.amount" var="amount" />
<fmt:message bundle="${loc}" key="local.text.name.description" var="description" />
<fmt:message bundle="${loc}" key="local.text.name.price" var="price" />
<fmt:message bundle="${loc}" key="local.text.name.image" var="image" />
<fmt:message bundle="${loc}" key="local.text.name.add" var="add" />
<fmt:message bundle="${loc}" key="local.exception.name.wrong_enter_parameters_exception" var="wrong_enter_parameters_exception" />



<form style="float: right;" action="${pageContext.request.contextPath}/controller" method="post">
		<input type="hidden" name="command" value="en"/>
		<input type="submit"  value="${en_button}"/>
</form>
	
<form  style="float: right;" action="${pageContext.request.contextPath}/controller" method="post">
		<input  type="hidden" name="command" value="ru"/>
		<input  type="submit"  value="${ru_button}"/>
</form>

<form  style="float: right;" action="${pageContext.request.contextPath}/controller" method="post">
		<input  type="hidden" name="command" value="logout_command"/>
		<input  type="submit"  value="${logout}"/>
</form>



<c:set var="message" value="${userMessage}"/>
<c:if test="${message !=null}"> <c:out value= "${greeting} ${message}"/></c:if>
<br/>
<br/>
<br/>
<center>
	<c:set var="error" value="${errorMessage}"/>
		<font color="red">
		<c:if test="${error !=null}">
			<c:if test="${error=='wrong_enter_parameters_exception'}">${wrong_enter_parameters_exception} </c:if>
		</c:if>
		</font>
</center>

<br/>

	<center>
		<c:set var="temp" value="${temp_message}"/>
		<c:if test="${temp !=null}">
			<c:if test="${temp=='success_add'}">${success_add} </c:if>
			<c:if test="${temp=='success_delete'}">${success_delete} </c:if>
			<c:if test="${temp=='success_change'}">${success_change} </c:if>

		</c:if>
	</center>
<br/>

<br/>


<form style="float: right;"  action="${pageContext.request.contextPath}/controller" method="get">
		<input  type="hidden"  name="command" value="build_admin_order_list"/>
	
		<input align="right"  type="submit"  value="${to_admin_orders_list}"/>
</form>
<form style="float: right;"  action="${pageContext.request.contextPath}/controller" method="get">
		<input  type="hidden"  name="command" value="build_black_list_page"/>
		<input align="right"  type="submit"  value="${to_black_list}"/>
</form>


<br/><br/>
<center>
		<table border="1" width="100%" cellpadding="5">
		
		
		
  		 <tr>
	    	 <td>${item_name}</td>
	    	 <td>${image}</td>
	    	 <td>${material}</td>
	    	 <td>${size}</td>
	    	 <td>${amount}</td>
	    	 <td>${description}</td>
	    	 <td>${price}</td>
	    	 <td>${add}</td>
	    	 

   		 </tr>
   		  <tr>
   			<form   action="${pageContext.request.contextPath}/controller" method="post">
   			 <input  type="hidden" name="command" value="add_new_item"/>
		 	 <td><input  type="text" size ="10" name="item_name" value=""/> </td>			
			 <td><input  type="text" name="item_image" value="/img/"/> </td>
			 <td><input  type="text" size ="5" name="item_material" value=""/> </td>			
			 <td><input  type="text" size ="3\" name="item_size" value=""/> </td>
			 <td><input  type="text" size ="3" name="item_amount" value=""/> </td>
			 <td><input  type="text" height="4" name="item_description" value=""/> </td>
			 <td><input  type="text" size ="3" name="item_price" value=""/> </td>
			 <td><input  type="submit"  value="${add_item}"/></td>
		  </form>
		  </tr>
   		 
   		 
	</table>
	</center>	
		<br/><br/><br/>

<center>
		<table border="1" width="100%" cellpadding="5">
		
		
		
  		 <tr>
    	 <td>${item_name}</td>
    	 <td>${image}</td>
    	 <td>${material}</td>
    	 <td>${size}</td>
    	 <td>${amount}</td>
    	 <td>${description}</td>
    	 <td>${price}</td>
    	 <td>${update}</td>
    	 <td>${delete}</td>

   		 </tr>
   		 
 
   		 
		 <c:if test="${sessionScope.totalUnits==null}">
   		 <c:set var="totalUnits" value="${sessionScope.totalUnits}"/>
   		 </c:if>
   		 
   		 <c:set var="shopList" value="${simpleinfo}"/>
   		 

		 <shop_tag:adminItemList itemList="${shopList}" updateButton="${update}"
		 contextPath="${pageContext.request.contextPath}" deleteButton="${delete}"  unitNumber="${totalUnits}">
		 </shop_tag:adminItemList>

 

</body>	
</html>