package by.epam.task.six.shop.shop_taglib;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.log4j.Logger;

import by.epam.task.six.shop.entity.ShopItem;
/**
 * Custom tag for user page.
 * 
 * Corresponds for right layout of elements on user page.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class ShopItemTag extends TagSupport {
	
	private ArrayList <ShopItem> shopItemList;
	private String contextPath;
	private String button;
	private int unitNumber;
	private int pages;

	private static Logger log = Logger.getLogger(ShopItemTag.class);
	
	
	public int getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(int unitNumber) {
		this.unitNumber = unitNumber;
	}
	
	public String getContextPath() {
		return contextPath;
	}
	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}
	public String getButton() {
		return button;
	}
	public void setButton(String button) {
		this.button = button;
	}
	public ArrayList <ShopItem> getShopItemList() {
		return shopItemList;
	}
	public void setShopItemList(ArrayList<ShopItem> shopItemList) {
		this.shopItemList = shopItemList;
		
	}
	
	
	public int doStartTag (){
		try {
			pageContext.getOut().write("<tr>");
			
		} catch (IOException e) {
			log.error("Error in writing from ShopItemTag.", e);
		};
		
		return EVAL_BODY_INCLUDE;}
	
	
	public int doAfterBody(){
		try {
			
		for(int i=0; i<shopItemList.size();i++){
			
			pageContext.getOut().write("<td>"+shopItemList.get(i).getName()+"</td>");
			pageContext.getOut().write("<td><img alt=\"image\" src=\""+contextPath+shopItemList.get(i).getImage()+"\"></td>");
			pageContext.getOut().write("<td>"+shopItemList.get(i).getMaterial()+"</td>");
			pageContext.getOut().write("<td>"+shopItemList.get(i).getSize()+"</td>");
			pageContext.getOut().write("<td>"+shopItemList.get(i).getAmount()+"</td>");
			pageContext.getOut().write("<td>"+shopItemList.get(i).getDescription()+"</td>");
			pageContext.getOut().write("<td>"+shopItemList.get(i).getPrice()+"</td>");
			pageContext.getOut().write("<td><form   action=\""+contextPath+"/controller\" method=\"post\">");
			pageContext.getOut().write("<input  type=\"hidden\" name=\"command\" value=\"set_item_to_shop_list\"/>");
			pageContext.getOut().write("<input  type=\"hidden\" name=\"item_id\" value=\""+shopItemList.get(i).getId()+"\"/>");
			pageContext.getOut().write("<input  type=\"submit\"  value=\""+button+"\"/>");
			pageContext.getOut().write("</form></td>");
			pageContext.getOut().write("<tr>");
			
			
			

		}
			

		} catch (IOException e) {
			log.error("Error in writing from ShopItemTag.", e);
		};

		
		return SKIP_BODY;}
	
	

	
	public int doEndTag(){
		try {
			
			if(unitNumber%5==0){
				pages=unitNumber/5;
			}else{
				pages=(unitNumber/5)+1;
			}
			
			pageContext.getOut().write("<tr>");
			pageContext.getOut().write("</table>");
			pageContext.getOut().write("</center>");
		
			pageContext.getOut().write("<table border=0 align=\"right\" >");

				for(int i = 1; i<pages+1;i++){
					
					pageContext.getOut().write("<td><form   action=\""+contextPath+"/controller\" method=\"post\">");
					pageContext.getOut().write("<input  type=\"hidden\" name=\"command\" value=\"set_current_page\"/>");
					pageContext.getOut().write("<input  type=\"hidden\" name=\"pageNumber\" value=\""+i+"\"/>");
					pageContext.getOut().write("<input  type=\"submit\"  value=\""+i+"\"/>");
					pageContext.getOut().write("</form></td>");
					

				}
				pageContext.getOut().write("</table>");
			
		} catch (IOException e) {
			log.error("Error in writing from ShopItemTag.", e);
		};
		return SKIP_BODY;}

}




