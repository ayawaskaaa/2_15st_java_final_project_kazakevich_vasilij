package by.epam.task.six.shop.shop_taglib;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.log4j.Logger;

import by.epam.task.six.shop.entity.ShopItem;
/**
 * Custom tag for admin page.
 * 
 * Corresponds for right layout of elements on admin page.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class AdminItemListTag extends TagSupport {
	
	private ArrayList <ShopItem> itemList;
	private String contextPath;
	private String deleteButton;
	private String updateButton;
	private int unitNumber;
	private int pages;

	private static Logger log = Logger.getLogger(ShopItemTag.class);
	
	
	public int getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(int unitNumber) {
		this.unitNumber = unitNumber;
	}
	
	public String getContextPath() {
		return contextPath;
	}
	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}

	public ArrayList <ShopItem> getitemList() {
		return itemList;
	}
	public void setitemList(ArrayList<ShopItem> itemList) {
		this.itemList = itemList;
		
	}
	public String getDeleteButton() {
		return deleteButton;
	}
	public void setDeleteButton(String deleteButton) {
		this.deleteButton = deleteButton;
	}
	public String getUpdateButton() {
		return updateButton;
	}
	public void setUpdateButton(String updateButton) {
		this.updateButton = updateButton;
	}


	
	
	public int doStartTag (){
		try {
			pageContext.getOut().write("<tr>");
			
		} catch (IOException e) {
			log.error("Error in writing from ShopItemTag.", e);
		};
		
		return EVAL_BODY_INCLUDE;}
	
	
	public int doAfterBody(){
		try {
			
		for(int i=0; i<itemList.size();i++){
			
			
			pageContext.getOut().write("<form   action=\""+contextPath+"/controller\" method=\"post\">");
			pageContext.getOut().write("<input  type=\"hidden\" name=\"command\" value=\"update_item\"/>");
			pageContext.getOut().write("<input  type=\"hidden\" name=\"item_id\" value=\""+itemList.get(i).getId()+"\"/>");
			pageContext.getOut().write("<td><input  type=\"text\" size =\"10\" name=\"item_name\" value=\""+itemList.get(i).getName()+"\"/> </td>");			
			pageContext.getOut().write("<td><img alt=\"image\" src=\""+contextPath+itemList.get(i).getImage()+"\"> </br>");
			pageContext.getOut().write("<input  type=\"text\" name=\"item_image\" value=\""+itemList.get(i).getImage()+"\"/> </td>");
			pageContext.getOut().write("<td><input  type=\"text\" size =\"5\" name=\"item_material\" value=\""+itemList.get(i).getMaterial()+"\"/> </td>");			
			pageContext.getOut().write("<td><input  type=\"text\" size =\"3\" name=\"item_size\" value=\""+itemList.get(i).getSize()+"\"/> </td>");
			pageContext.getOut().write("<td><input  type=\"text\" size =\"3\" name=\"item_amount\" value=\""+itemList.get(i).getAmount()+"\"/> </td>");
			pageContext.getOut().write("<td><input  type=\"text\" height=\"4\" name=\"item_description\" value=\""+itemList.get(i).getDescription()+"\"/> </td>");
			pageContext.getOut().write("<td><input  type=\"text\" size =\"3\" name=\"item_price\" value=\""+itemList.get(i).getPrice()+"\"/> </td>");
			pageContext.getOut().write("<td><input  type=\"submit\"  value=\""+updateButton+"\"/>");
			pageContext.getOut().write("</form>");
			
			pageContext.getOut().write("<td><form   action=\""+contextPath+"/controller\" method=\"post\">");
			pageContext.getOut().write("<input  type=\"hidden\" name=\"command\" value=\"delete_item\"/>");
			pageContext.getOut().write("<input  type=\"hidden\" name=\"item_id\" value=\""+itemList.get(i).getId()+"\"/>");
			pageContext.getOut().write("<input  type=\"submit\"  value=\""+deleteButton+"\"/>");
			pageContext.getOut().write("</form></td>");
			
			pageContext.getOut().write("<tr>");
			
			
			
			
			

		}
			

		} catch (IOException e) {
			log.error("Error in writing from ShopItemTag.", e);
		};

		
		return SKIP_BODY;}
	
	

	
	public int doEndTag(){
		try {
			
			if(unitNumber%5==0){
				pages=unitNumber/5;
			}else{
				pages=(unitNumber/5)+1;
			}
			
			pageContext.getOut().write("<tr>");
			pageContext.getOut().write("</table>");
			pageContext.getOut().write("</center>");
		

			pageContext.getOut().write("<table border=0 align=\"right\" >");

			for(int i = 1; i<pages+1;i++){
				
				pageContext.getOut().write("<td><form   action=\""+contextPath+"/controller\" method=\"post\">");
				pageContext.getOut().write("<input  type=\"hidden\" name=\"command\" value=\"set_current_page\"/>");
				pageContext.getOut().write("<input  type=\"hidden\" name=\"pageNumber\" value=\""+i+"\"/>");
				pageContext.getOut().write("<input  type=\"submit\"  value=\""+i+"\"/>");
				pageContext.getOut().write("</form></td>");
				

			}
			pageContext.getOut().write("</table>");

			
		} catch (IOException e) {
			log.error("Error in writing from ShopItemTag.", e);
		};
		return SKIP_BODY;}

}


