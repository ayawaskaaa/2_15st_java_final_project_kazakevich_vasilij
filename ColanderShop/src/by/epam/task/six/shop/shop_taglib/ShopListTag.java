package by.epam.task.six.shop.shop_taglib;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

import by.epam.task.six.shop.entity.ShopItem;
/**
 * Custom tag for shop list page.
 * 
 * Corresponds for right layout of elements on shop list page.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class ShopListTag extends TagSupport {
	
	private ArrayList <ShopItem> shopList;
	private String contextPath;
	private String orderButton;
	private String deleteButton;
	private int unitNumber;
	private int pages;
	private static Logger log = Logger.getLogger(ShopItemTag.class);
	
	
	public String getDeleteButton() {
		return deleteButton;
	}
	public void setDeleteButton(String deleteButton) {
		this.deleteButton = deleteButton;
	}
	
	
	public int getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(int unitNumber) {
		this.unitNumber = unitNumber;
	}
	
	public String getContextPath() {
		return contextPath;
	}
	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}
	public String getOrderButton() {
		return orderButton;
	}
	public void setOrderButton(String orderButton) {
		this.orderButton = orderButton;
	}
	public ArrayList <ShopItem> getshopList() {
		return shopList;
	}
	public void setshopList(ArrayList<ShopItem> shopList) {
		this.shopList = shopList;
		
	}
	
	
	public int doStartTag (){
		try {
			pageContext.getOut().write("<tr>");
			
		} catch (IOException e) {
			log.error("Error in writing from ShopItemTag.", e);
		};
		
		return EVAL_BODY_INCLUDE;}
	
	
	public int doAfterBody(){
		try {
			
		for(int i=0; i<shopList.size();i++){
			
			StringBuilder amount = new StringBuilder();
			for(int z=1; z<shopList.get(i).getAmount()+1; z++ ){amount.append("<option>"+z+"</option>");}
			
			pageContext.getOut().write("<td>"+shopList.get(i).getName()+"</td>");
			pageContext.getOut().write("<td><img alt=\"image\" src=\""+contextPath+shopList.get(i).getImage()+"\"></td>");
			pageContext.getOut().write("<td>"+shopList.get(i).getMaterial()+"</td>");
			pageContext.getOut().write("<td>"+shopList.get(i).getSize()+"</td>");
			pageContext.getOut().write("<td>"+shopList.get(i).getAmount()+"</td>");
			pageContext.getOut().write("<td>"+shopList.get(i).getDescription()+"</td>");
			pageContext.getOut().write("<td>"+shopList.get(i).getPrice()+"</td>");
			pageContext.getOut().write("<td><form   action=\""+contextPath+"/controller\" method=\"post\">");
			pageContext.getOut().write("<input  type=\"hidden\" name=\"command\" value=\"make_an_order\"/>");
			pageContext.getOut().write("<select name=\"choosed_amount\" >");
			pageContext.getOut().write(amount.toString());
			pageContext.getOut().write("</select>");
			pageContext.getOut().write("<input  type=\"hidden\" name=\"item_id\" value=\""+shopList.get(i).getId()+"\"/>");
			pageContext.getOut().write("<input  type=\"hidden\" name=\"price\" value=\""+shopList.get(i).getPrice()+"\"/>");
			pageContext.getOut().write("<input  type=\"submit\"  value=\""+orderButton+"\"/>");
			pageContext.getOut().write("</form></td>");
			pageContext.getOut().write("<td><form   action=\""+contextPath+"/controller\" method=\"post\">");
			pageContext.getOut().write("<input  type=\"hidden\" name=\"command\" value=\"delete_from_shop_list\"/>");
			pageContext.getOut().write("<input  type=\"hidden\" name=\"item_id\" value=\""+shopList.get(i).getId()+"\"/>");
			pageContext.getOut().write("<input  type=\"submit\"  value=\""+deleteButton+"\"/>");
			pageContext.getOut().write("</form></td>");
			pageContext.getOut().write("<tr>");
			
			
			

		}
			

		} catch (IOException e) {
			log.error("Error in writing from ShopItemTag.", e);
		};

		
		return SKIP_BODY;}
	
	

	
	public int doEndTag(){
		try {
			
			if(unitNumber%5==0){
				pages=unitNumber/5;
			}else{
				pages=(unitNumber/5)+1;
			}
			
			pageContext.getOut().write("<tr>");
			pageContext.getOut().write("</table>");
			pageContext.getOut().write("</center>");
		

			pageContext.getOut().write("<table border=0 align=\"right\" >");

			for(int i = 1; i<pages+1;i++){
				
				pageContext.getOut().write("<td><form   action=\""+contextPath+"/controller\" method=\"post\">");
				pageContext.getOut().write("<input  type=\"hidden\" name=\"command\" value=\"set_current_page\"/>");
				pageContext.getOut().write("<input  type=\"hidden\" name=\"pageNumber\" value=\""+i+"\"/>");
				pageContext.getOut().write("<input  type=\"submit\"  value=\""+i+"\"/>");
				pageContext.getOut().write("</form></td>");
				

			}
			pageContext.getOut().write("</table>");

			
		} catch (IOException e) {
			log.error("Error in writing from ShopItemTag.", e);
		};
		return SKIP_BODY;}

}

