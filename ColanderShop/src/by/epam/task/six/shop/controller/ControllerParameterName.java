package by.epam.task.six.shop.controller;

/**
 * Definition of request parameters.
 *
 *All request parameters are contained here.
 *
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public final class ControllerParameterName {

	private ControllerParameterName(){}
		public static final String COMMAND_NAME= "command";
		public static final String SIMPLE_INFO = "simpleinfo";
		public static final String FILE_NAME = "filename";
		public static final String ERROR = "error";
		public static final String BUILD_USER_PAGE = "build_user_page";		
		public static final String SET_ITEM_TO_SHOP_LIST = "set_item_to_shop_list";
		public static final String DELETE_ITEM = "delete_item";
		public static final String UPDATE_ITEM = "update_item";
		public static final String ADD_NEW_ITEM = "add_new_item";
		public static final String BUILD_BLACK_LIST_PAGE = "build_black_list_page";
		public static final String BUILD_ADMIN_ORDER_LIST = "build_admin_order_list";
		public static final String SET_TO_BLACK_LIST = "set_to_black_list";
		public static final String DELETE_FROM_BLACK_LIST = "delete_from_black_list";
		public static final String CHANGE_ORDER_STATE = "change_order_state";
		public static final String BUILD_ADMIN_PAGE = "build_admin_page";		
		public static final String SHOW_SHOP_LIST = "show_shop_list";
		public static final String DELETE_FROM_SHOP_LIST = "delete_from_shop_list";
		public static final String MAKE_AN_ORDER = "make_an_order";
		public static final String PAY_FOR_ORDER = "pay_for_order";
		public static final String BUILD_ORDER_LIST = "build_order_list";
		

	
}
