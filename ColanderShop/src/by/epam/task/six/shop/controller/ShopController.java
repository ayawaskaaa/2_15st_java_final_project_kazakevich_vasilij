package by.epam.task.six.shop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.exception.CommandException;

/**
 * Main application servlet.
 * 
 * Handles users requests. If user uses get request it will be executed by doPost method. After success execution 
 * user is forwarded to new jsp page. But if user will use post request - he will be redirected to new URL after 
 * successful request execution.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class ShopController extends HttpServlet {
	private static final long SerialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(ShopController.class);

	public ShopController() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		
		String commandName = request.getParameter(ControllerParameterName.COMMAND_NAME);
		String page = null;

		try {
			ICommand command;
			command = CommandHelper.getInstance().getCommand(commandName);
			page = command.execute(request, response);			
			RequestDispatcher dispatcher = request.getRequestDispatcher(page);
			
			dispatcher.forward(request, response);
			
			
		} catch (CommandException e) {
		
			log.error(e);
			errorMessageDireclyFromresponse(response);
		} catch (NullPointerException e){
			
			log.error("Dispatcher is null.", e);
			errorMessageDireclyFromresponse(response);
			
		} catch (ServletException e) {			
			log.error("ServletException was caused, while using dispatcher.",e);
			errorMessageDireclyFromresponse(response);
			
		} catch (IOException e) {			
			log.error("IOException was caused, while using dispatcher.", e);
			errorMessageDireclyFromresponse(response);
			
		}catch (Exception e) {
			
			log.error(e);
			errorMessageDireclyFromresponse(response);
		} 

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) {

		String commandName = request
				.getParameter(ControllerParameterName.COMMAND_NAME);
		String page = null;

		try {
			ICommand command;
			command = CommandHelper.getInstance().getCommand(commandName);
			page = command.execute(request, response);
			response.sendRedirect(page);

			
		} catch (CommandException e) {
			
			log.error(e);
			errorMessageDireclyFromresponse(response);
		} catch (NullPointerException e){
			
			log.error("Dispatcher is null.", e);
			errorMessageDireclyFromresponse(response);
			
		}  catch (IOException e) {			
			log.error("IOException was caused, while using dispatcher.", e);
			errorMessageDireclyFromresponse(response);
			
		}catch (Exception e) {			
			log.error(e);		
			errorMessageDireclyFromresponse(response);
		} 


	}

	
	private void errorMessageDireclyFromresponse(HttpServletResponse response) {
		
		response.setContentType("text/html");
		
		try {
			response.getWriter().println("E R R O R");
		} catch (IOException e) {
			log.error("Error in servlet writing. " + e);
			
		}

	}
}
