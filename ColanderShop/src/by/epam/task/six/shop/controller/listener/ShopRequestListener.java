package by.epam.task.six.shop.controller.listener;


import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;

import by.epam.task.six.shop.service.ServiceRequestParameterName;
/**
 *Saves previous users request.
 *
 *Saves previous users request to session. 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class ShopRequestListener implements ServletRequestListener{	


	@Override
	public void requestInitialized(ServletRequestEvent req) {
		HttpServletRequest request = (HttpServletRequest) req.getServletRequest();
		if(request.getHeader(ServiceRequestParameterName.REFERER)!=null){
			saveRequestUrl(request);
			}
		
	}
	
	@Override
	public void requestDestroyed(ServletRequestEvent req) {

			
	}

	
	private void saveRequestUrl(HttpServletRequest request){
		
			if(!request.getRequestURL().toString().equals(request
					.getHeader(ServiceRequestParameterName.REFERER))){
				
			request.getSession().setAttribute(ServiceRequestParameterName.PREVIOUS_PAGE,
					request.getHeader(ServiceRequestParameterName.REFERER));
			}
	}

}
