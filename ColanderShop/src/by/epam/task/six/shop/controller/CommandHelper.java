package by.epam.task.six.shop.controller;

import java.util.HashMap;
import java.util.Map;

import by.epam.task.six.shop.service.CommandName;
import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.exception.CommandException;
import by.epam.task.six.shop.service.impl.AddNewItem;
import by.epam.task.six.shop.service.impl.BuildAdminOrderPage;
import by.epam.task.six.shop.service.impl.BuildAdminPage;
import by.epam.task.six.shop.service.impl.BuildBlackListPage;
import by.epam.task.six.shop.service.impl.BuildUserOrderPage;
import by.epam.task.six.shop.service.impl.BuildUserPage;
import by.epam.task.six.shop.service.impl.BuildVisitorPage;
import by.epam.task.six.shop.service.impl.ChangeLocaleCommand;
import by.epam.task.six.shop.service.impl.ChangeOrderState;
import by.epam.task.six.shop.service.impl.DeleteFromBlackList;
import by.epam.task.six.shop.service.impl.DeleteFromShopList;
import by.epam.task.six.shop.service.impl.DeleteItemFromDb;
import by.epam.task.six.shop.service.impl.LoginCommand;
import by.epam.task.six.shop.service.impl.LogoutCommand;
import by.epam.task.six.shop.service.impl.MakeAnOrder;
import by.epam.task.six.shop.service.impl.NoSuchCommand;
import by.epam.task.six.shop.service.impl.PayForOrder;
import by.epam.task.six.shop.service.impl.RegisterCommand;
import by.epam.task.six.shop.service.impl.SetCurrentPage;
import by.epam.task.six.shop.service.impl.SetItemToShopList;
import by.epam.task.six.shop.service.impl.SetToBlackList;
import by.epam.task.six.shop.service.impl.ShowShopList;
import by.epam.task.six.shop.service.impl.ToLoginPage;
import by.epam.task.six.shop.service.impl.ToRegisterPage;
import by.epam.task.six.shop.service.impl.UpdateItem;
/**
 * Links enter command names with implementations of ICommand interface. 
 * 
 * Class has public method which returns an instance of it.
 * Class serves to return copies of implementations of ICommand interface, 
 * in addiction to enter command names. 
 * 
 *
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public final class CommandHelper {

	private static final CommandHelper instance = new CommandHelper();

	private final Map<CommandName, ICommand> commands = new HashMap<>();

	public CommandHelper() {
		commands.put(CommandName.BUILD_USER_PAGE, new BuildUserPage());
		commands.put(CommandName.BUILD_ADMIN_PAGE, new BuildAdminPage());
		commands.put(CommandName.BUILD_ORDER_LIST, new BuildUserOrderPage());
		commands.put(CommandName.BUILD_BLACK_LIST_PAGE, new BuildBlackListPage());
		commands.put(CommandName.BUILD_ADMIN_ORDER_LIST, new BuildAdminOrderPage());
		commands.put(CommandName.NO_SUCH_COMMAND, new NoSuchCommand());
		commands.put(CommandName.LOGIN_COMMAND, new LoginCommand());
		commands.put(CommandName.REGISTER_COMMAND, new RegisterCommand());
		commands.put(CommandName.TO_LOGIN_PAGE, new ToLoginPage());
		commands.put(CommandName.TO_REGISTER_PAGE, new ToRegisterPage());
		commands.put(CommandName.LOGOUT_COMMAND, new LogoutCommand());
		commands.put(CommandName.EN, new ChangeLocaleCommand());
		commands.put(CommandName.RU, new ChangeLocaleCommand());	
		commands.put(CommandName.BUILD_VISITOR_PAGE, new BuildVisitorPage());
		commands.put(CommandName.SET_CURRENT_PAGE, new SetCurrentPage());
		commands.put(CommandName.SET_ITEM_TO_SHOP_LIST, new SetItemToShopList());
		commands.put(CommandName.SHOW_SHOP_LIST, new ShowShopList());
		commands.put(CommandName.MAKE_AN_ORDER, new MakeAnOrder());
		commands.put(CommandName.DELETE_FROM_SHOP_LIST,	new DeleteFromShopList());		
		commands.put(CommandName.PAY_FOR_ORDER,	new PayForOrder());		
		commands.put(CommandName.DELETE_ITEM,	new DeleteItemFromDb());
		commands.put(CommandName.UPDATE_ITEM,	new UpdateItem());
		commands.put(CommandName.ADD_NEW_ITEM,	new AddNewItem());
		commands.put(CommandName.SET_TO_BLACK_LIST,	new SetToBlackList());
		commands.put(CommandName.DELETE_FROM_BLACK_LIST, new DeleteFromBlackList());
		commands.put(CommandName.CHANGE_ORDER_STATE, new ChangeOrderState());

	}

	public static CommandHelper getInstance() throws CommandException {

		return instance;
	}

	public ICommand getCommand(String commandName) {
		CommandName name = CommandName.valueOf(commandName.toUpperCase());
		ICommand command;
		if (null != name) {
			command = commands.get(name);
		} else {
			command = commands.get(CommandName.NO_SUCH_COMMAND);
		}
		return command;

	}

}
