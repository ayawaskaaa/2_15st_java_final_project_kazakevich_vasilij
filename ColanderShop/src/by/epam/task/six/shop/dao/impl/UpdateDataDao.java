package by.epam.task.six.shop.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import by.epam.task.six.shop.dao.UpdateDataAction;
import by.epam.task.six.shop.dao.connection_pool.ConnectionPoolException;
import by.epam.task.six.shop.dao.connection_pool.ShopConnectionPool;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.entity.ShopItem;
/**
 * Interface for updating data in data repository.
 * 
 * Takes parameters and replaces old data by new.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class UpdateDataDao implements UpdateDataAction {

	private static final String SQL_UPDATE_PAY_FOR_ORDER = "UPDATE orders SET state = '�������' WHERE id_user = ? AND id = ? ;";
	private static final String SQL_UPDATE_ITEM = "UPDATE item SET name = ? , image = ? ,"
			+ " material = ? , size = ? , amount = ? , description = ? , price = ? WHERE id = ? ;";
	private static final String SQL_UPDATE_ORDER_STATE = "UPDATE orders SET state = ?  WHERE id = ? ;";
	
	private static final UpdateDataDao instance = new UpdateDataDao();

	@Override
	public void changeState(String state, int idOrder)	throws ActionDaoException {
		
		Connection con = null;
		PreparedStatement changeStateStatement = null;
		final ShopConnectionPool shopConnectionPool = ShopConnectionPool.getInstance();

		try {

			con = shopConnectionPool.takeConnection();
			changeStateStatement = con.prepareStatement(SQL_UPDATE_ORDER_STATE);
			changeStateStatement.setString(1, state);
			changeStateStatement.setInt(2, idOrder);

			changeStateStatement.executeUpdate();

		} catch (ConnectionPoolException e) {
			throw new ActionDaoException("Exception while getting pool connection in UpdateDataDao", e);
		} catch (SQLException e) {

			throw new ActionDaoException(
					"Exception while changing order state in UpdateDataDao", e);
		} finally {

				shopConnectionPool.closeConnection(con, changeStateStatement);
			
		}

	}

	@Override
	public void updateItem(ShopItem item) throws ActionDaoException {
		Connection con = null;
		PreparedStatement updateStatement = null;
		final ShopConnectionPool shopConnectionPool = ShopConnectionPool.getInstance();
		
		try {
			con = shopConnectionPool.takeConnection();
			updateStatement = con.prepareStatement(SQL_UPDATE_ITEM);
			updateStatement.setString(1, item.getName());
			updateStatement.setString(2, item.getImage());
			updateStatement.setString(3, item.getMaterial());
			updateStatement.setInt(4, item.getSize());
			updateStatement.setInt(5, item.getAmount());
			updateStatement.setString(6, item.getDescription());
			updateStatement.setInt(7, item.getPrice());
			updateStatement.setInt(8, item.getId());

			updateStatement.executeUpdate();

		} catch (SQLException e) {
			System.out.println(e);
			throw new ActionDaoException(
					"Exception while statement creation in UpdateItemDao.", e);

		} catch (ConnectionPoolException e) {

			throw new ActionDaoException(
					"Exception while getting pool connection in UpdateItemDao",
					e);

		} finally {

				shopConnectionPool.closeConnection(con, updateStatement);
			
		}

	}

	@Override
	public void payForOrder(int idUser, int idOrder) throws ActionDaoException {
		Connection con = null;
		PreparedStatement payStatement = null;
		int user = idUser;
		int order = idOrder;
		final ShopConnectionPool shopConnectionPool = ShopConnectionPool
				.getInstance();

		try {
			con = shopConnectionPool.takeConnection();
			payStatement = con.prepareStatement(SQL_UPDATE_PAY_FOR_ORDER);
			payStatement.setInt(1, idUser);
			payStatement.setInt(2, order);
			payStatement.executeUpdate();

		} catch (ConnectionPoolException e) {
			throw new ActionDaoException(
					"Exception while getting pool connection", e);
		} catch (SQLException e) {
			throw new ActionDaoException(
					"SQLException in PayForOrderDao class.", e);
		} finally {

				shopConnectionPool.closeConnection(con, payStatement);


		}

	}

	public static UpdateDataAction getInstance() throws ActionDaoException {
		
		return instance;	
	}
}
