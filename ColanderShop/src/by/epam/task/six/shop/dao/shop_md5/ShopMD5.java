package by.epam.task.six.shop.dao.shop_md5;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
/**
 * Hash algorithm.
 * 
 * Takes String parameter, encrypts with  use of MD5 algorithm
 * and returns back.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public final class ShopMD5 {
public static final String getHash(String str) {
        
        MessageDigest md5 ;        
        StringBuffer  hexString = new StringBuffer();
        
        try {
                                    
            md5 = MessageDigest.getInstance("md5");
            
            md5.reset();
            md5.update(str.getBytes()); 
                        
                        
            byte messageDigest[] = md5.digest();
                        
            for (int i = 0; i < messageDigest.length; i++) {
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            }
                                                                                        
        } 
        catch (NoSuchAlgorithmException e) {                        
            return e.toString();
        }
        
        return hexString.toString();
    }
    
   

}
