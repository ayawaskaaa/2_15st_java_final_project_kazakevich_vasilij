package by.epam.task.six.shop.dao.connection_pool;

import by.epam.task.six.shop.controller.exception.ControllerException;
/**
 * Exception of ShopConnectionPool class.
 * 
 * All exceptions in ShopConnectionPool class should be warped in to
 *  ConnectionPoolException exception.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class ConnectionPoolException extends ControllerException {

private static final long serialVersionUID = 1L;
	
	public ConnectionPoolException(String msg){
		super(msg);
	}

	public ConnectionPoolException(String msg, Exception e ){
		super(msg, e);
	}

}
