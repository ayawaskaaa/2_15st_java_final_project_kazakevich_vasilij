package by.epam.task.six.shop.dao.exception;

import by.epam.task.six.shop.controller.exception.ControllerException;

/**
 * DAO layer exception.
 *  
 *All exceptions in DAO layer should be wrapped in ActionDaoException. 
 *DAO layer should throw only CommandException type exceptions.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */

public class ActionDaoException extends ControllerException{
	
	private static final long serialVersionUID=1L;
	
	public ActionDaoException(String msg) {
		super(msg);
	}

	public ActionDaoException(String msg, Exception e) {
		super(msg, e);
	}

	
	
	

}
