package by.epam.task.six.shop.dao;

import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.entity.ShopUser;
/**
 * Interface contains tools related to authorization.
 * 
 * Takes parameters for login or register actions, collect data for response and
 * returns object of ShopUser type.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public interface AuthorizationAction {
	
	public ShopUser registerNewUser(String name, String password, String mail ) throws ActionDaoException;
	
	public ShopUser login(String login, String password) throws ActionDaoException;
}
