package by.epam.task.six.shop.service;
/**
 * Definition of service layer parameters.
 * 
 * All service layer parameters are contained here.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public final class ServiceRequestParameterName {
	
	private ServiceRequestParameterName(){}
	
	public static final String ITEM_NAME = "item_name";
	public static final String ITEM_IMAGE = "item_image";
	public static final String ITEM_ID = "item_id";
	public static final String ITEM_MATERIAL = "item_material";
	public static final String ITEM_SIZE = "item_size";
	public static final String ITEM_AMOUNT = "item_amount";
	public static final String ITEM_DESCRIPTION = "item_description";
	public static final String ITEM_PRICE = "item_price";
	public static final String ERROR_MESSAGE = "errorMessage";
	public static final String USER_MESSAGE = "userMessage";
	public static final String PREVIOUS_PAGE = "previousPage";
	public static final String WRONG_ENTER_PARAMETERS_EXCEPTION = "wrong_enter_parameters_exception";
	public static final String ORDER_STATE_BUTTON = "order_state_button";
	public static final String ORDER_ID = "order_id";
	public static final String USER_ID = "user_id";
	public static final String BLOCK_ID = "block_id";
	public static final String PAGE_NUMBER = "pageNumber";
	public static final String USER_INFO = "userInfo";
	public static final String CHOOSED_AMOUNT = "choosed_amount";
	public static final String PRICE = "price";
	public static final String SHOP_LIST = "shopList";
	public static final String TOTAL_UNITS = "totalUnits";
	public static final String PAGE_FLAG = "page_flag";
	public static final String FROM_FIRST_PAGE = "0";
	public static final String REFERER = "Referer";
	public static final String LOCAL = "local";
	public static final String VISITOR_URL = "http://localhost:8080/ColanderShop/";
	public static final String TEMP_MESSAGE = "temp_message";
	public static final Object SUCCESS_ADD = "success_add";
	public static final Object SUCCESS_DELETE = "success_delete";
	public static final Object SUCCESS_ORDER = "success_order";
	public static final String ALSO = "?";
	public static final String AND = "&";
	public static final String IS = "=";
	public static final String SUCCESS_PAY = "success_pay";
	public static final String BUILD_USER_PAGE_REQUEST = "?command=build_user_page";
	public static final String BUILD_ADMIN_PAGE_REQUEST = "?command=build_admin_page";
	public static final String BUILD_VISITOR_PAGE_REQUEST ="?command=build_visitor_page";
	public static final String REDIRECT_TO_LOGIN_PAGE = "?command=to_login_page";
	public static final String REDIRECT_TO_REGISTER_PAGE = "?command=to_register_page";
	public static final String TO_SHOP_LIST = "?command=show_shop_list";
	public static final String TO_ORDER_LIST = "?command=build_order_list";
	public static final String SUCCESS_CHANGE = "success_change";
	public static final String BUILD_BLACK_LIST_PAGE = "?command=build_black_list_page";
	public static final String BUILD_ADMIN_ORDER_PAGE = "?command=build_admin_order_list";
	


	
	

}
