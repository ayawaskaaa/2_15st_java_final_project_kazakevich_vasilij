package by.epam.task.six.shop.service.impl;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;
/**
 * Deletes item from shop list.
 *  
 * If user places item in to shop list, id of item is put in to users session.
 * If user deletes item from shop list , id of item is deleted from session. 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class DeleteFromShopList implements ICommand {

	private static final String URL_TO_SHOP_LIST_DELETE = "?command=show_shop_list"
			+ "&temp_message=success_delete";
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
			List shopList = new ArrayList();
			
			shopList=(List) request.getSession().getAttribute(ServiceRequestParameterName.SHOP_LIST);
			if(shopList.contains(request.getParameter(ServiceRequestParameterName.ITEM_ID))){
			shopList.remove(request.getParameter(ServiceRequestParameterName.ITEM_ID));
			}
			request.getSession().setAttribute(ServiceRequestParameterName.SHOP_LIST, shopList);
			return request.getRequestURL().toString()+URL_TO_SHOP_LIST_DELETE;	

	}	
	
}

