package by.epam.task.six.shop.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.dao.InsertDataAction;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.dao.impl.InsertDataDao;
import by.epam.task.six.shop.entity.ShopUser;
import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;
/**
 * Creates a user order.
 *  
 * After enter parameter check, with the help of makeAnOrderDao class 
 * links user with item and its parameters in data repository. After order action item, which was ordered, should
 * be deleted from users shop list.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class MakeAnOrder implements ICommand {
	
	private static final String URL_TO_SHOP_LIST_ORDER = "?command=show_shop_list"
			+ "&temp_message=success_order";

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

		int itemId;
		int userId;
		int amount;
		int priceForItem;
		ShopUser user;
		List shopList;

		InsertDataAction makeAnOrderDao = null;

		parametersValidation(request);

		try {
			
			user = (ShopUser) request.getSession().getAttribute(ServiceRequestParameterName.USER_INFO);
			userId = new Integer(user.getId());
			amount = Integer.parseInt(request.getParameter(ServiceRequestParameterName.CHOOSED_AMOUNT));
			itemId = Integer.parseInt(request.getParameter(ServiceRequestParameterName.ITEM_ID));
			priceForItem = Integer.parseInt(request.getParameter(ServiceRequestParameterName.PRICE));

			makeAnOrderDao = InsertDataDao.getInstance();

			makeAnOrderDao.makeAnOrder(userId, itemId, amount, priceForItem);

			shopList = (List) request.getSession().getAttribute(ServiceRequestParameterName.SHOP_LIST);

			shopList.remove(new Integer(itemId).toString());

			request.getSession().setAttribute(ServiceRequestParameterName.SHOP_LIST, shopList);
			
			return request.getRequestURL().toString()+URL_TO_SHOP_LIST_ORDER;
			

		} catch (ActionDaoException e) {
			throw new CommandException("Exception in MakeAnOrder class", e);
		}

	

	}

	private void parametersValidation(HttpServletRequest request)
			throws CommandException {

		if (request.getSession().getAttribute(
				ServiceRequestParameterName.USER_INFO) == null
				|| request.getParameter(ServiceRequestParameterName.CHOOSED_AMOUNT) == null
				|| request.getParameter(ServiceRequestParameterName.ITEM_ID) == null
				|| request.getParameter(ServiceRequestParameterName.PRICE) == null) {

			throw new CommandException("Enter parameters are incorrect.");

		}

	}

}
