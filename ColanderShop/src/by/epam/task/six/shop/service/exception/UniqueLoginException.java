package by.epam.task.six.shop.service.exception;

import by.epam.task.six.shop.controller.exception.ControllerException;
/**
 * Service layer exception.
 *  
 * If user wants to use for registration login that already exists,  UniqueLoginException should be thrown.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class UniqueLoginException extends ControllerException{
	
	private static final long serialVersionUID=1L;
	
	public UniqueLoginException(String msg) {
		super(msg);
	}

	public UniqueLoginException(String msg, Exception e) {
		super(msg, e);
	}

	
	
	

}
