package by.epam.task.six.shop.service.exception;

import by.epam.task.six.shop.controller.exception.ControllerException;
/**
 * Service layer exception.
 *  
 * If enter parameters don't pass validation, WrongAuthorizationException should be thrown.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class WrongAuthorizationException extends ControllerException {

private static final long serialVersionUID=1L;
	
	public WrongAuthorizationException(String msg) {
		super(msg);
	}

	public WrongAuthorizationException(String msg, Exception e) {
		super(msg, e);
	}

	
}
