package by.epam.task.six.shop.service.impl;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.controller.ControllerParameterName;
import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;
/**
 * Changes users current locale.
 *  
 * Contains method for changing current locale. Sets new locale in to users Session and returns
 * previous redirect location URL of user.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class ChangeLocaleCommand implements ICommand {
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		String page = null;

		

			request.getSession().setAttribute(ServiceRequestParameterName.LOCAL, 
					request.getParameter(ControllerParameterName.COMMAND_NAME));	
			
			return request.getSession().getAttribute(ServiceRequestParameterName.PREVIOUS_PAGE).toString();
		
	}

}
