package by.epam.task.six.shop.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.controller.ControllerParameterName;
import by.epam.task.six.shop.controller.JspPageName;
import by.epam.task.six.shop.entity.BlackListBean;
import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;
import by.epam.task.six.shop.service.util.BlackListPageProvider;
/**
 * Prepares all information for building black list page.
 *  
 * Contains method for getting array of BlackList objects and making them available for further usage.  
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class BuildBlackListPage implements ICommand {

	@Override
	public String execute(HttpServletRequest request,HttpServletResponse response) throws CommandException {
		
		List<BlackListBean> blackList=null;
		BlackListPageProvider blProvider = BlackListPageProvider.getInstance();
		
		blackList=blProvider.getBlackList(request, response);
		request.setAttribute(ControllerParameterName.SIMPLE_INFO, blackList);
		if(request.getParameter(ServiceRequestParameterName.TEMP_MESSAGE)!=null){
			request.setAttribute(ServiceRequestParameterName.TEMP_MESSAGE,
					request.getParameter(ServiceRequestParameterName.TEMP_MESSAGE));
		}
		return JspPageName.BLACK_LIST_PAGE;
	}

}
