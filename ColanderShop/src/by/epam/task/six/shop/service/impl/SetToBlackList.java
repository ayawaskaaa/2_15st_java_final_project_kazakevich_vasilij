package by.epam.task.six.shop.service.impl;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.dao.InsertDataAction;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.dao.impl.InsertDataDao;
import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;
/**
 * Sets user to black list
 * 
 * Inputs id of user into data repository.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class SetToBlackList implements ICommand {
	
	private static final String URL_TO_ADMIN_ORDER_PAGE_ADD = "?command=build_admin_order_list&temp_message=success_add";
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		
		int userId;
		InsertDataAction setToBLDao;
		try{userId = Integer.parseInt(request.getParameter(ServiceRequestParameterName.USER_ID));
		
		
		setToBLDao = InsertDataDao.getInstance();
		setToBLDao.setUserToBlackList(userId);
		return request.getRequestURL()+ URL_TO_ADMIN_ORDER_PAGE_ADD;
		
		}catch(NumberFormatException e){
			throw new CommandException("User id is null",e);
		} catch (ActionDaoException e) {
			throw new CommandException("Exception while getting dao.",e);
		}
		
	}

}
