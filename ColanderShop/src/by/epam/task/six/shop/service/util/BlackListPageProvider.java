package by.epam.task.six.shop.service.util;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.controller.JspPageName;
import by.epam.task.six.shop.dao.GetBeanAction;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.dao.impl.GetBeanDao;
import by.epam.task.six.shop.entity.BlackListBean;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;
/**
 * Gets BlackListBean objects from data repository.
 * 
 * Helps service layer classes to get array of BlackListBean type objects.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class BlackListPageProvider  {
	
	private static final BlackListPageProvider instance = new BlackListPageProvider();
	
	public static BlackListPageProvider getInstance(){
		return instance;
	}


	public List<BlackListBean> getBlackList(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		int pageNumber;

		GetBeanAction getBlackListDao;
		int allShopItems = 0;
		List<BlackListBean> blackList;

		try {
			getBlackListDao = GetBeanDao.getInstance();

			openPageFromBegining(request);

			allShopItems = getBlackListDao.countUsersInBlackList();

			setPageNumberToSession(request);

			setToSessionTotalNumberOfUnits(allShopItems, request);

			if (request.getSession().getAttribute(ServiceRequestParameterName.PAGE_NUMBER) == null) {
				pageNumber = 0;
				blackList = getBlackListDao.getBlackList(pageNumber);

				return blackList;

			} else {
				pageNumber = countPageNumbers(Integer.parseInt(request.getSession()
						.getAttribute(ServiceRequestParameterName.PAGE_NUMBER).toString()));
				blackList = getBlackListDao.getBlackList(pageNumber);
				return blackList;
				

			}

		} catch (ActionDaoException e) {
			throw new CommandException(	"Exception while getting GetBlackListDao.", e);
		}

	}

	private void setToSessionTotalNumberOfUnits(Object totalItemCount,
			HttpServletRequest request) {
		request.getSession().setAttribute(ServiceRequestParameterName.TOTAL_UNITS, totalItemCount);

	}

	private int countPageNumbers(int number) {
		if (number == 1) {
			return 0;
		} else {
			int pageNumber = (number - 1) * 5;
			return pageNumber;
		}
	}

	private void setPageNumberToSession(HttpServletRequest request) {
		if (request.getAttribute(ServiceRequestParameterName.PAGE_NUMBER) != null) {
			request.getSession().setAttribute(ServiceRequestParameterName.PAGE_NUMBER,
							request.getAttribute(ServiceRequestParameterName.PAGE_NUMBER));
		}
	}

	private void openPageFromBegining(HttpServletRequest request) {
		if (request.getSession().getAttribute(ServiceRequestParameterName.PAGE_FLAG) != null) {

			if (!request.getSession().getAttribute(ServiceRequestParameterName.PAGE_FLAG)
					.equals(JspPageName.BLACK_LIST_PAGE)) {
				
				request.getSession().setAttribute(ServiceRequestParameterName.PAGE_NUMBER,
								request.getParameter(ServiceRequestParameterName.FROM_FIRST_PAGE));
				
				request.getSession().setAttribute(ServiceRequestParameterName.PAGE_FLAG,
						JspPageName.BLACK_LIST_PAGE);
			}

		} else {
			request.getSession().setAttribute(ServiceRequestParameterName.PAGE_NUMBER,
							request.getParameter(ServiceRequestParameterName.FROM_FIRST_PAGE));
			
			request.getSession().setAttribute(ServiceRequestParameterName.PAGE_FLAG,
					JspPageName.BLACK_LIST_PAGE);
		}
	}
}
