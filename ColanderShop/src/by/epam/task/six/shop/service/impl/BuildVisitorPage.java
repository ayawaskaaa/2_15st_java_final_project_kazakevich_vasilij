package by.epam.task.six.shop.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.controller.JspPageName;
import by.epam.task.six.shop.controller.ControllerParameterName;
import by.epam.task.six.shop.dao.GetBeanAction;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.dao.impl.GetBeanDao;
import by.epam.task.six.shop.entity.ShopItem;
import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;
/**
 * Prepares all information for building visitor page.
 *  
 * Contains method for getting array of ShopItams objects and making them available for further usage.  
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class BuildVisitorPage implements ICommand {

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) throws CommandException {
		int pageNumber;
		String page = JspPageName.VISITOR_PAGE;
		GetBeanAction pageInfoDao = null;
		List<ShopItem> shopItemList;
		int allShopItems = 0;

		try {
			pageInfoDao = GetBeanDao.getInstance();

			openPageFromBegining(request);
			allShopItems = pageInfoDao.countShopItems();

			setToSessionTotalNumberOfUnits(allShopItems, request);
			if(request.getParameter(ServiceRequestParameterName.USER_MESSAGE)!=null){
				request.setAttribute(ServiceRequestParameterName.USER_MESSAGE,
						request.getParameter(ServiceRequestParameterName.USER_MESSAGE) );
			}

			if (request.getParameter(ServiceRequestParameterName.PAGE_NUMBER) != null) {
				request.getSession()
						.setAttribute(
								ServiceRequestParameterName.PAGE_NUMBER,
								request.getParameter(ServiceRequestParameterName.PAGE_NUMBER));
			}

			if (request.getSession().getAttribute(
					ServiceRequestParameterName.PAGE_NUMBER) == null) {
				pageNumber = 0;
				shopItemList = pageInfoDao.getItemsForMainPage(pageNumber);

				request.setAttribute(ControllerParameterName.SIMPLE_INFO,
						shopItemList);

			} else {

				pageNumber = countPageNumbers(Integer.parseInt(request
						.getSession()
						.getAttribute(ServiceRequestParameterName.PAGE_NUMBER)
						.toString()));

				shopItemList = pageInfoDao.getItemsForMainPage(pageNumber);

				request.setAttribute(ControllerParameterName.SIMPLE_INFO,
						shopItemList);

			}
		} catch (ActionDaoException e) {
			throw new CommandException("Excection in getting DAO", e);
		}

		return page;
	}

	private void setToSessionTotalNumberOfUnits(int allShopItems,
			HttpServletRequest request) {
		request.getSession().setAttribute(
				ServiceRequestParameterName.TOTAL_UNITS, allShopItems);

	}

	private int countPageNumbers(int number) {

		if (number == 1) {

			return 0;
		} else {
			int pageNumber = (number - 1) * 5;

			return pageNumber;
		}
	}

	private void openPageFromBegining(HttpServletRequest request) {
		if (request.getSession().getAttribute(
				ServiceRequestParameterName.PAGE_FLAG) != null) {

			if (!request.getSession()
					.getAttribute(ServiceRequestParameterName.PAGE_FLAG)
					.equals(JspPageName.VISITOR_PAGE)) {

				request.getSession()
						.setAttribute(
								ServiceRequestParameterName.PAGE_NUMBER,
								request.getParameter(ServiceRequestParameterName.FROM_FIRST_PAGE));

				request.getSession().setAttribute(
						ServiceRequestParameterName.PAGE_FLAG,
						JspPageName.VISITOR_PAGE);
			}

		} else {
			request.getSession()
					.setAttribute(
							ServiceRequestParameterName.PAGE_NUMBER,
							request.getParameter(ServiceRequestParameterName.FROM_FIRST_PAGE));

			request.getSession().setAttribute(
					ServiceRequestParameterName.PAGE_FLAG,
					JspPageName.VISITOR_PAGE);

		}
	}

}
