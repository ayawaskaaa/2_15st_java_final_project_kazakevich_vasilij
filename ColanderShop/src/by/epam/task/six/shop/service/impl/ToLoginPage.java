package by.epam.task.six.shop.service.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.controller.JspPageName;
import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;
/**
 * Redirects user to login page. 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class ToLoginPage implements ICommand {

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) throws CommandException {
		
		if(request.getParameter(ServiceRequestParameterName.ERROR_MESSAGE)!=null){
			request.setAttribute(ServiceRequestParameterName.ERROR_MESSAGE,
					request.getParameter(ServiceRequestParameterName.ERROR_MESSAGE));
		}
		return JspPageName.LOGIN_PAGE;
	}

}
