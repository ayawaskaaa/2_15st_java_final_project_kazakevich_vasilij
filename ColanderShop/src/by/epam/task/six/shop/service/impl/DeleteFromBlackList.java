package by.epam.task.six.shop.service.impl;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.dao.DeleteDataAction;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.dao.impl.DeleteDataDao;
import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;
/**
 * Deletes user from black list.
 *  
 * Takes parametersTo determine user to delete and does it with the help of  DeleteDataDao class instance.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class DeleteFromBlackList implements ICommand {
	
	private static final String URL_TO_BLACK_LIST_PAGE_DELETE = "?command=build_black_list_page"
			+ "&temp_message=success_delete";

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		
		int blockId;
		DeleteDataAction deleteFromBLDao;
		
		try {
			
			blockId = Integer.parseInt(request.getParameter(ServiceRequestParameterName.BLOCK_ID));
			deleteFromBLDao=DeleteDataDao.getInstance();
			
			deleteFromBLDao.deleteFromBlackList(blockId);
			
			return request.getRequestURL()+ URL_TO_BLACK_LIST_PAGE_DELETE;	
			
			
		} catch (ActionDaoException e) {
			throw new CommandException("Exception in DeleteFromBlackList while geting DAO. ",e);
		} catch (NumberFormatException e){
			
			throw new CommandException("Exception in DeleteFromBlackList while geting blockId. ",e);
			
		} 
	}

}
