package by.epam.task.six.shop.service.impl;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;
/**
 * Sets current users page number.
 * 
 * If user changes page number, it sets to users session.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */

public class SetCurrentPage implements ICommand {

	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		
		
			request.getSession().setAttribute(ServiceRequestParameterName.PAGE_NUMBER, request.getParameter(ServiceRequestParameterName.PAGE_NUMBER));
			
			return request.getSession().getAttribute(ServiceRequestParameterName.PREVIOUS_PAGE).toString();


	}

	
	
}
