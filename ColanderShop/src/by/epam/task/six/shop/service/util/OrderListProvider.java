package by.epam.task.six.shop.service.util;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.controller.JspPageName;
import by.epam.task.six.shop.controller.ControllerParameterName;
import by.epam.task.six.shop.dao.GetBeanAction;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.dao.impl.GetBeanDao;
import by.epam.task.six.shop.entity.ShopUser;
import by.epam.task.six.shop.entity.UserOrder;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;
/**
 * Gets UserOrder objects from data repository.
 * 
 * Helps service layer classes to get array of UserOrder type objects.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class OrderListProvider  {
	
	private static OrderListProvider instance = new OrderListProvider();
	
	public static OrderListProvider getInstance(){
		return instance;
	};


	public List<UserOrder> getOrderList(HttpServletRequest request,	HttpServletResponse response) throws CommandException {

		int pageNumber;
		GetBeanAction orderListDao = null;
		int userId = 0;
		List<UserOrder> orderList;
		int allShopItems = 0;
		ShopUser user;

		user = (ShopUser) request.getSession().getAttribute(ServiceRequestParameterName.USER_INFO);

		userId = new Integer(user.getId());

		try {

			orderListDao = GetBeanDao.getInstance();

			allShopItems = orderListDao.countUserOrders(userId);

			openPageFromBegining(request);

			setToSessionTotalNumberOfUnits(allShopItems, request);

			if (request.getSession().getAttribute(ServiceRequestParameterName.PAGE_NUMBER) == null) {
				pageNumber = 0;
				orderList = orderListDao.getUserOrders(userId, pageNumber);

				request.setAttribute(ControllerParameterName.SIMPLE_INFO,orderList);
				request.setAttribute(
						ServiceRequestParameterName.USER_MESSAGE,request.getParameter(ServiceRequestParameterName.USER_MESSAGE));
				return orderList;
			
			} else {
				pageNumber = countPageNumbers(Integer.parseInt(request.getSession()
						.getAttribute(ServiceRequestParameterName.PAGE_NUMBER).toString()));
				orderList = orderListDao.getUserOrders(userId, pageNumber);

				return orderList;
			}
			


		} catch (ActionDaoException e) {
			throw new CommandException(	"Exception while geting dao in BuildOrderList.", e);

		}

	}

	private void setToSessionTotalNumberOfUnits(Object totalItemCount,
			HttpServletRequest request) {
		request.getSession().setAttribute(ServiceRequestParameterName.TOTAL_UNITS, totalItemCount);

	}

	private int countPageNumbers(int number) {
		if (number == 1) {
			return 0;
		} else {
			int pageNumber = (number - 1) * 5;
			return pageNumber;
		}
	}

	private void openPageFromBegining(HttpServletRequest request) {
		if (request.getSession().getAttribute(ServiceRequestParameterName.PAGE_FLAG) != null) {
			
			if(!request.getSession().getAttribute(ServiceRequestParameterName.PAGE_FLAG)
					.equals(JspPageName.ORDER_PAGE)){
				request.getSession().setAttribute(ServiceRequestParameterName.PAGE_NUMBER,
						request.getParameter(ServiceRequestParameterName.FROM_FIRST_PAGE));
				request.getSession().setAttribute(ServiceRequestParameterName.PAGE_FLAG,
						JspPageName.ORDER_PAGE);
				
			}
			
		
		}else{
			request.getSession().setAttribute(ServiceRequestParameterName.PAGE_NUMBER,
					request.getParameter(ServiceRequestParameterName.FROM_FIRST_PAGE));
			request.getSession().setAttribute(ServiceRequestParameterName.PAGE_FLAG,
					JspPageName.ORDER_PAGE);
			
		}
	}

}
