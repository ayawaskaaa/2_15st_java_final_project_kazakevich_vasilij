package by.epam.task.six.shop.service.impl;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.dao.AuthorizationAction;
import by.epam.task.six.shop.dao.DaoParemeterName;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.dao.impl.AuthorizationDao;
import by.epam.task.six.shop.dao.shop_md5.ShopMD5;
import by.epam.task.six.shop.entity.ShopItem;
import by.epam.task.six.shop.entity.ShopUser;
import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;
import by.epam.task.six.shop.service.exception.ServiceExceptionName;
import by.epam.task.six.shop.service.exception.WrongAuthorizationException;
import by.epam.task.six.shop.service.util.AdminPageProvider;
import by.epam.task.six.shop.service.util.UserPageProvider;

/**
 * Performs login action.
 * 
 * Checks enter parameters, if they are not null, try to get ShopUser object from data repository.
 * Makes ShopUser object available for further usage. If parameter check or getting ShopUser object failed,
 * the exception should be thrown. 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class LoginCommand implements ICommand {
	
	private static final String URL_TO_USER_PAGE = "?command=build_user_page";
	private static final String URL_TO_ADMIN_PAGE = "?command=build_admin_page";
	private static final String URL_TO_LOGIN_PAGE_AUTHORIZATION = "?command=to_login_page"
			+ "&errorMessage=authorizationexception";

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

		String password;
		String login;
		String page = null;
		UserPageProvider userPageProvider = null;
		AdminPageProvider adminPageProvider = null;
		AuthorizationAction loginDao = null;
		List<ShopItem> shopItemList;
		ShopUser user;
		

		login = request.getParameter(DaoParemeterName.LOGIN);
		password = ShopMD5.getHash(request.getParameter(DaoParemeterName.PASSWORD));

		try {
			if (login == null || password == null) {

				throw new WrongAuthorizationException("Enter parameters are null");

			}

			loginDao = AuthorizationDao.getInstance();

			user = (ShopUser) loginDao.login(login, password);


			if (user.getIdRole() == 2) {
				
				request.getSession(true).setAttribute(request.getSession().getId(), 2);
				request.getSession(true).setAttribute(ServiceRequestParameterName.USER_INFO, user);
				
				return page=request.getRequestURL().toString()+URL_TO_ADMIN_PAGE;
				
			}else if (user.getIdRole() == 1) {

				request.getSession(true).setAttribute(request.getSession().getId(), 1);
				request.getSession(true).setAttribute(ServiceRequestParameterName.USER_INFO, user);				
				
				return page=request.getRequestURL().toString()+URL_TO_USER_PAGE;

			} else {				
				
				return page = request.getRequestURL().toString()+URL_TO_LOGIN_PAGE_AUTHORIZATION;
						

			}

		} catch (WrongAuthorizationException e) {
			
			 page = request.getRequestURL().toString()+URL_TO_LOGIN_PAGE_AUTHORIZATION;
			
		} catch (ActionDaoException e) {
			
			 page = request.getRequestURL().toString()+URL_TO_LOGIN_PAGE_AUTHORIZATION;
			
		}
		return page;

	}
}
