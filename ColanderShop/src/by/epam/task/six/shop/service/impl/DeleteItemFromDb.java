package by.epam.task.six.shop.service.impl;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.dao.DeleteDataAction;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.dao.impl.DeleteDataDao;
import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;
/**
 * Deletes item from data repository.
 *  
 * Deletes item with the help of DeleteDataDao class instance by item id.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class DeleteItemFromDb implements ICommand{
	
	private static final String URL_TO_ADMIN_PAGE_DELETE = "?command=build_admin_page"
			+ "&temp_message=success_delete";

	@Override
	public String execute(HttpServletRequest request,HttpServletResponse response) throws CommandException {
		
		int itemId;
		DeleteDataAction deleteDao;		
		
		try {
			itemId = Integer.parseInt(request.getParameter(ServiceRequestParameterName.ITEM_ID));
			deleteDao = DeleteDataDao.getInstance();
			deleteDao.deleteItem(itemId);
			
			return request.getRequestURL()+ URL_TO_ADMIN_PAGE_DELETE;	
				
			
			
			} catch (ActionDaoException e) {
			throw new CommandException("Exception in DeleteItemFromDb while geting DAO. ",e);		

			}

	}
}
