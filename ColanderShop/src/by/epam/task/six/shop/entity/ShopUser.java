package by.epam.task.six.shop.entity;
/**
 * Project JavaBean component.
 * 
 * Contains components of shop user.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class ShopUser {
	
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public int getIdRole() {
		return idRole;
	}
	public void setIdRole(int idRole) {
		this.idRole = idRole;
	}
	private String login;
	private String mail;
	private int idRole;
	

}
