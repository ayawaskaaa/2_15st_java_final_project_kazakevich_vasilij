package test.by.epam.task.six.shop.dao.impl;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import by.epam.task.six.shop.dao.DeleteDataAction;
import by.epam.task.six.shop.dao.connection_pool.ConnectionPoolException;
import by.epam.task.six.shop.dao.connection_pool.ShopConnectionPool;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.dao.impl.DeleteDataDao;

public class DeleteDataDaoTest {

	private static ShopConnectionPool shopConnectionPool = null;
	private final String SQL_COUNT_BLOCKS_IN_BL = "SELECT COUNT(id_block) FROM blacklist;";
	private final String SQL_INSERT_USER_IN_BL = "INSERT INTO blacklist (id_user) values (4) ;";
	private final String SQL_SELECT_ID_BLOCK = "SELECT id_block from blacklist WHERE id_user = 4 ;";

	private final String SQL_COUNT_ITEMS = "SELECT COUNT(name) FROM item ;";
	private final String SQL_SELECT_ITEM_ID = "SELECT id from item where name = 'testItem'";
	private final String SQL_INSERT_NEW_ITEM = "INSERT INTO item (name, size, material, price,  image, amount, description)"
			+ "values ('testItem', 0, 'test', 0, 'test', 0, 'test' ) ;";

	@BeforeClass
	public static void prepeareConnection() throws ConnectionPoolException {

		shopConnectionPool = ShopConnectionPool.getInstance();
		shopConnectionPool.initPoolData();

	}

	@AfterClass
	public static void disposeConnectionPool() throws ConnectionPoolException {

		shopConnectionPool.dispose();
	}

	@Test
	public void deleteFromBlackListTest() throws ActionDaoException,
			ConnectionPoolException, SQLException {
		DeleteDataAction deleteFromBL = DeleteDataDao.getInstance();
		Connection con = null;
		ResultSet rs = null;
		Statement countStatement = null;
		Statement insertStatement = null;
		Statement selectStatement = null;
		int afterDelete = 0;
		int beforeDelete = 0;
		int blockId = 0;

		try {

			con = shopConnectionPool.takeConnection();

			countStatement = con.createStatement();
			insertStatement = con.createStatement();
			selectStatement = con.createStatement();

			insertStatement.executeUpdate(SQL_INSERT_USER_IN_BL);
			rs = countStatement.executeQuery(SQL_COUNT_BLOCKS_IN_BL);

			while (rs.next()) {
				beforeDelete = rs.getInt(1);
			}

			rs = selectStatement.executeQuery(SQL_SELECT_ID_BLOCK);
			while (rs.next()) {
				blockId = rs.getInt(1);
			}

			deleteFromBL.deleteFromBlackList(blockId);

			rs = countStatement.executeQuery(SQL_COUNT_BLOCKS_IN_BL);

			while (rs.next()) {
				afterDelete = rs.getInt(1);
			}

			assertEquals("Item wasn't deleted .", afterDelete, beforeDelete - 1);

		} catch (ConnectionPoolException e) {
			throw new ConnectionPoolException(
					"Exception while taking pool connection.", e);
		} catch (SQLException e) {
			throw new SQLException("Error in deleteFromBlackListTest.", e);
		} finally {
		
				shopConnectionPool.closeConnection(countStatement);
				shopConnectionPool.closeConnection(selectStatement);
				shopConnectionPool.closeConnection(con, insertStatement, rs);
			}
		}

	

	@Test
	public void deleteItemTest() throws ActionDaoException,	ConnectionPoolException, SQLException {
		DeleteDataAction deleteFromBL = DeleteDataDao.getInstance();
		Connection con = null;
		ResultSet rs = null;
		Statement countStatement = null;
		Statement insertStatement = null;
		Statement selectStatement = null;
		int afterDelete = 0;
		int beforeDelete = 0;
		int itemId = 0;

		try {
			con = shopConnectionPool.takeConnection();
			countStatement = con.createStatement();
			insertStatement = con.createStatement();
			selectStatement = con.createStatement();

			selectStatement.executeUpdate(SQL_INSERT_NEW_ITEM);

			rs = countStatement.executeQuery(SQL_COUNT_ITEMS);

			while (rs.next()) {
				beforeDelete = rs.getInt(1);
			}

			rs = selectStatement.executeQuery(SQL_SELECT_ITEM_ID);
			while (rs.next()) {
				itemId = rs.getInt(1);
			}

			deleteFromBL.deleteItem(itemId);

			rs = countStatement.executeQuery(SQL_COUNT_ITEMS);

			while (rs.next()) {
				afterDelete = rs.getInt(1);
			}

			assertEquals("Item wasn't deleted .", afterDelete, beforeDelete - 1);

		} catch (ConnectionPoolException e) {
			throw new ConnectionPoolException("Exception while taking pool connection.", e);
		} catch (SQLException e) {
			throw new SQLException("Error in deleteItemTest.", e);
		} finally {
		
				shopConnectionPool.closeConnection(countStatement);
				shopConnectionPool.closeConnection(selectStatement);
				shopConnectionPool.closeConnection(con, insertStatement, rs);
		
		}

	}

}
