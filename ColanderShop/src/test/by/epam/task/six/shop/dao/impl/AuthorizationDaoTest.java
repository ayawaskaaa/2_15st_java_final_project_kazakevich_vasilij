package test.by.epam.task.six.shop.dao.impl;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import by.epam.task.six.shop.dao.AuthorizationAction;
import by.epam.task.six.shop.dao.connection_pool.ConnectionPoolException;
import by.epam.task.six.shop.dao.connection_pool.ShopConnectionPool;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.dao.impl.AuthorizationDao;
import by.epam.task.six.shop.entity.ShopUser;

public class AuthorizationDaoTest {

	private final String TESTNAME = "testUser";
	private final String PASSWORD = "TestUser1";
	private final String MAIL = "TestUser@gmail.com";
	private final String SQL_SELECT_TEST_USER = "SELECT password, login, mail FROM user WHERE id = ? ;";
	private final static String SQL_DELETE_TEST_USER = "DELETE FROM user WHERE id = ? ;";

	
	private static ShopConnectionPool shopConnectionPool = null;

	@BeforeClass
	public static void prepeareConnection() throws ConnectionPoolException {

		shopConnectionPool = ShopConnectionPool.getInstance();
		shopConnectionPool.initPoolData();

	}

	@Test
	public void registerTest() throws ActionDaoException, SQLException,	ConnectionPoolException {
		 Connection con = null;
		 ResultSet rs = null;
		 PreparedStatement selectStatement = null;
		 PreparedStatement deleteTestUserStatement = null;

		AuthorizationAction autTest = AuthorizationDao.getInstance();
		int userId = 0;
		ShopUser user = new ShopUser();
		user = autTest.registerNewUser(TESTNAME, PASSWORD, MAIL);
		userId = user.getId();
		String login = null;
		String password = null;
		String mail = null;
		con = shopConnectionPool.takeConnection();

		selectStatement = con.prepareStatement(SQL_SELECT_TEST_USER);
		selectStatement.setInt(1, user.getId());
		rs = selectStatement.executeQuery();
		while (rs.next()) {
			password = rs.getString(1);
			login = rs.getString(2);
			mail = rs.getString(3);
		}

		deleteTestUserStatement = con.prepareStatement(SQL_DELETE_TEST_USER);
		deleteTestUserStatement.setInt(1, userId);
		deleteTestUserStatement.executeUpdate();
		shopConnectionPool.closeConnection(deleteTestUserStatement);
		shopConnectionPool.closeConnection(con, selectStatement, rs);

		assertEquals("Login, is not equals.", TESTNAME, login);
		assertEquals("Password is not equals.", PASSWORD, password);
		assertEquals("Mail is not equals.", MAIL, mail);

	}

	@Test(expected = ActionDaoException.class)
	public void registerExceptionTest() throws ActionDaoException, SQLException, ConnectionPoolException {
		 Connection con = null;
		 ResultSet rs = null;
		 PreparedStatement selectStatement = null;
		 PreparedStatement deleteTestUserStatement = null;

		AuthorizationAction autTest = AuthorizationDao.getInstance();
		int userId = 0;
		ShopUser user = new ShopUser();
		con = shopConnectionPool.takeConnection();
		user = autTest.registerNewUser(TESTNAME, PASSWORD, MAIL);

		try {
			user = autTest.registerNewUser(TESTNAME, PASSWORD, MAIL);
		} catch (ActionDaoException e) {
			userId = user.getId();

			deleteTestUserStatement = con.prepareStatement(SQL_DELETE_TEST_USER);

			deleteTestUserStatement.setInt(1, userId);
			deleteTestUserStatement.executeUpdate();
			throw new ActionDaoException("Exception has thrown.", e);
		} finally {

			shopConnectionPool.closeConnection(con, deleteTestUserStatement);
		}

	}

	@Test
	public void loginTest() throws ActionDaoException, ConnectionPoolException,	SQLException {
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement selectStatement = null;
		PreparedStatement deleteTestUserStatement = null;
		AuthorizationAction autTest = AuthorizationDao.getInstance();
		int userId = 0;
		ShopUser registerUser = new ShopUser();
		ShopUser loginUser = new ShopUser();
		registerUser = autTest.registerNewUser(TESTNAME, PASSWORD, MAIL);
		userId = registerUser.getId();

		con = shopConnectionPool.takeConnection();

		loginUser = autTest.login(TESTNAME, PASSWORD);

		deleteTestUserStatement = con.prepareStatement(SQL_DELETE_TEST_USER);

		deleteTestUserStatement.setInt(1, userId);
		deleteTestUserStatement.executeUpdate();

		shopConnectionPool.closeConnection(con, deleteTestUserStatement);

		assertEquals("Login, is not equals.", TESTNAME, loginUser.getLogin());

	}

	@Test(expected = ActionDaoException.class)
	public void loginExceptionTest() throws ActionDaoException {
		AuthorizationAction autTest = AuthorizationDao.getInstance();
		autTest.login(TESTNAME, PASSWORD);

	}

	@AfterClass
	public static void disposeConnectionPool() throws ConnectionPoolException {

		shopConnectionPool.dispose();
	}

}
